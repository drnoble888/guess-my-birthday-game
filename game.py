from random import randint

users_name = input("Hi! What is your name? ")

months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

for guess_number in range(1, 6):
    rand_month = randint(1, 12)
    rand_year = randint(1924, 2004)
    print("Guess number:", guess_number, users_name, ",were you born in", months[rand_month - 1], "/", rand_year, "?")
    user_input = input("yes or no? ")
    if user_input == "no" and guess_number < 5: 
        print("Drat! Lemme try again!")
        guess_number += 1
    elif user_input == "yes":
        print("I knew it!")
        break
    else:
        print("I have other things to do. Good bye.")
